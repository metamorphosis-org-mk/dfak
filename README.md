[[_TOC_]]

Digital First Aid Kit
============

The [Digital First Aid Kit](https://rarenet.gitlab.io/dfak) aims at providing initial support for people facing the most common types of digital threats. *The Digital First Aid Kit is not meant to serve as the ultimate solution to all your digital emergencies.*

The [website](https://rarenet.gitlab.io/dfak/) deployed from this repository offers a series of [diagnostic workflows](content/en/topics/) for a series of frequent digital security issues that can be harder to analyze and, therefore, to address.

By following these workflows, the reader can analyze their issue step by step until they reach a diagnosis, with tips on how to address their digital security emergencies, or on whom they can reach out to to solve their issues.

The Digital First Aid Kit is addressed at rapid responders, digital security trainers, and activists who support their community in the digital self-defense sphere.

About
-----

The Digital First Aid Kit is a collaborative effort of a number of organizations and individuals in the [Rapid Response Community](https://rarenet.org) and beyond - see our [Contributors list](/CONTRIBUTORS.md).


## Repo structure

```
content/en
├── index
│   ├── intro.md
│   ├── ...
├── organisations
│   ├── access_now.md
│   └── greenhost.md
├── contact-methods
│   ├── email.md
│   ├── pgp.md
│   └── ...
└── topics
    ├── account-access-issues.md
    ├── device-acting-suspiciously.md
    ├── device-seized.md
    ├── harassed-online.md
    ├── website-not-working.md
    └── ...
```

 - The editorial content is in the `/content` folder.
 - Each language has the same structure as the above `/content/en`

The Digital First Aid Kit contains different kinds of content, with their specific format and structure:

- [Topics](content/en/topics) - Diagnostic workflows for addressing digital security emergencies
- [Contact Methods](content/en/contact-methods) - Description of the various contact methods accepted by the organizations listed in the website, and warnings on risks connected to their usage.
- [Organizations](content/en/organisations) - Member organizations of the Rapid Response Network that offer support in case of emergencies.


How to Contribute
--------------

 - Create an account on Gitlab.com if you don't already have one
 - Create a fork of this repository
 - Edit files
 - Ensure that the site builds correctly at `https://MY_NAME.gitlab.io/dfak/` (change MY_NAME to your gitlab user account).
 - When your work is ready, submit a Merge Request
 - The Digital First Aid Kit editors will review your changes and comment on your Merge Request, possibly asking you to make some edits
 - Once the Merge Request is satisfying to all, it will be approved and merged
 - You contribution is now accepted

Please make sure to always fill in the meta tags in the YAML front matter, as this contains important information for the deployment of the website.

While the body of the files in the "Contact Methods" and "Organizations" folder should only contain a description of the contact methods or the organization respectively, the content of the "Topics" folder follows a concatenation of diagnostic steps that lead to different possible endings. What follows is a description of how topics are structured. Please read this carefully if you would like to contribute to the Topics section.


### Contribute Topics

The "Topics" folder contains interactive diagnostic workflows on several digital security issues that rapid responders encounter often in their work. Each diagnostic workflow consists in a concatenation of steps that start from a symptom (e.g. "I can't access the internet", or "I can't reach my website") and, based on the choices made in each screen, help the reader identify the nature of the issue they are facing and offer recommendations to address their issue. In case no diagnosis is reached, the reader is presented with a list of organizations that can help them mitigate their emergency. All workflows end with a series of tips to reduce the risk of the issue arising again.

To contribute a new topic, please follow this structure:

1. Introduction - one or two paragraphs that explain what the issue might be about.
2. Workflow - a series of questions aimed at identifying the cause of the issue, starting from the most urgent, for example to rule out a life-threatening situation or potential legal problems for the affected person.

    Answers to each question should either lead to a new question or to a section with recommendations.

    Sections with recommendations should end with feedback questions on whether the recommendations worked or not:

    - a "no" answer to this question should lead to:
        1. more questions in case the recommendations didn't work and a different diagnosis is still possible, or
        2. referrals to organizations specializing in the mitigation of the diagnosed problem
    - a "yes" answer to this question should lead to the final section with recommendations.

You can find an example of the syntax and of the organization of workflows in this [Workflow Template](workflow_template.md).

### Localize the Website

If you want to add a new language to the Digital First Aid Kit, please follow the instructions in the "Contribute Topics" above, as well as the style guidelines and technical instructions below.

Translations should always be reviewed before they are published: please plan for a revision of your translations by native speakers who are familiar with the topics addressed in the DFAK. If needed, we can help find someone who can do this revision - please let us know by writing to dfak @ digitaldefenders . org.

#### Style guidelines

- We would like the language to be as gender-neutral as possible. In English, this means never using "he" or "him" as the universal pronoun, and to use "they" instead. If a gender-neutral solution is really hard to get in your target language, we would also appreciate a translation that privileges the feminine over the masculine. The point is to not assume that the DFAK audience or tech professionals are all male.
- We would appreciate it if you could "localize" the DFAK rather than just "translate" it - especially with regard to the linked
resources, but also with regard to some procedures, there might be something that applies much better to your region than what you find in the English text. So please feel free to replace links to English resources with links to maintained and authoritative resources in languages spoken or understood by your audience, as well as to suggest us tweaks in the procedures that would make more sense to you - we will be happy to discuss these together - just write to us at dfak @ digitaldefenders . org.


#### How to translate the Digital First Aid Kit

If you want to add a new language to the Digital First Aid Kit, what you will need to do is, fundamentally, create a new folder with your language code in the [`content` folder](https://gitlab.com/rarenet/dfak/-/blob/master/content/), and add there the translation of all files included in the [`content/en` folder](https://gitlab.com/rarenet/dfak/-/tree/master/content/en).

##### What to translate

In the files included in the `topics` folder, there are some titles that should not be translated, as they have a placeholder function and are not shown in the website as text.

- You can localize the title, at the beginning of each topic, with just 1 `#`.
- Please don't localize any of the tags with hashes after that, including ## Workflow and ### start
- It's better also not to localize the titles of the ### tags, as they aren't rendered in the website and have just a placeholder function.
- Feel free to translate anything starting with 4 or more hashes (e.g. #### Resources should be translated).


It's also important to pay attention to the values (the part after the `:`) in the YAML front matter that also need to be translated in the text files, for the following keys:

```
title: *translate the value*
language: *replace with your language code*
summary: *translate the value*
permalink: *replace the language code in the path*
parent: *translate the value*
```

and in the organizations/ folder:

```
hours: *translate the value*
response_time: *translate the value*
```

To localize menus, buttons, etc., you will also need to translate the values (the part after the `:`) in the [`strings`](https://gitlab.com/rarenet/dfak/-/blob/master/preview.yml#L28) and [`tags`](https://gitlab.com/rarenet/dfak/-/blob/master/preview.yml#L180) sections in the [`preview.yml`](https://gitlab.com/rarenet/dfak/-/blob/master/preview.yml) file. To do so, add  to both sections a new list starting with your language code and containing the translated values.

For ease, below you can find a table with all the strings that need to be translated:

|  Key *(should not be translated)* |  Value *(should be translated)* |
|--------------------------------|------------------------------|
| answer: | "Now, answer the following questions to get a better idea of what is going on." |
| start | "Start" |
| previous_question | "Previous question" |
| more_tips | "More tips" |
| resources | "Resources" |
| expand_info | "Expand info" |
| fold | "Fold" |
| consider | "Consider" |
| support | "Find support" |
| about | "About" |
| self-care | "Self care" |
| offline_version | "Download website" |
| gitlab_repo | "Gitlab repo" |
| breadcrumb_dfak | "DFAK" |
| topics | "Topics" |
| languages | "Languages" |
| beneficiaries | "Beneficiaries" |
| services | "Services" |
| website | "Website" |
| who_they_help | "Who they help" |
| how_to_contact | "How to contact" |
| available_in_languages | "Available in languages" |
| working_hours | "Working hours" |
| expected_response_time | "Expected response time" |
| triage | Initial Triage |
| grants_funding | Grants & Funding |
| in_person_training | In-Person Training |
|  org_security | Organizational Security |
| web_hosting | Website Hosting |
| web_protection | Website Protection (Denial of Service Protection) |
| digital_support | 24/7 digital support |
|  relocation | Relocation |
|  physical_sec | Physical Security |
|  equipment_replacement| Equipment replacement |
|  assessment | Assessing Threats and Risks |
| secure_comms | Securing Communications |
|  device_security | Device Security |
|  vulnerabilities_malware | Vulnerabilities and Malware |
|  browsing | Web Browsing Security |
|  account | Account Security |
|  harassment | Online Harassment Mitigation (Doxxing, Trolling) |
|  forensic | Forensic Analysis |
|  legal | Legal Support |
|  individual_care | Individual care |
|  advocacy | Public advocacy for individuals (arrested, in prison, etc.) |
|  ddos | Denial of Service Protection |
|  censorship | Censorship circumvention |
|  journalists | Journalists |
|  hrds | Human Rights Defenders |
|  hros | Human rights organisations at risk working in their countries |
|  activists | Activists |
|  lgbti | LGBTI Groups |
|  women | Women |
|  youth | Youth |
|  cso | Civil Society Organisations |
|  land | Land Defenders |
|  indigenous | Indigenous organizations |
|  media | Independent media |
|  web_form | Web Form |
|  email | Email |
|  pgp | PGP |
|  pgp_key | Public Key URL |
|  pgp_key_fingerprint | PGP Key Fingerprint |
|  mail | Post |
|  signal | Signal |
|  skype | Skype |
|  whatsapp | WhatsApp |
|  phone | Phone |
|  tor | Tor |
|  telegram | Telegram |


***Thanks for your contribution to the Digital First Aid Kit!***



License
-------

Content is licensed as [Creative Commons BY-SA 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/).
Code is licensed as [GNU General Public License version 3](https://www.gnu.org/copyleft/gpl.html) - {confirm}
