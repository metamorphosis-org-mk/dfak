---
layout: sidebar.pug
title: "Ndjeheni të tronditur?"
author: FP
language: sq
summary: "Ngacmimet në internet, kërcënimet dhe llojet e tjera të sulmeve digjitale mund të krijojnë ndjenja të tronditjes dhe gjendje emocionale shumë delikate: mund të ndjeheni fajtorë, të turpëruar, të shqetësuar, të zemëruar, konfuz, të pafuqishëm, apo edhe mund të keni frikë për mirëqenien tuaj psikologjike ose fizike."
date: 2018-09-05
permalink: /sq/self-care/
parent: Home
sidebar: >
  <h3>Lexoni më shumë rreth mënyrës sesi të mbroheni nga ndjenjat shqetësuese:</h3>

  <ul>
    <li><a href="https://www.newtactics.org/conversation/self-care-activists-sustaining-your-most-valuable-resource">Vet-kujdesi për aktivistët: Ruajtja e burimit tuaj më të vlefshëm</a></li>
    <li><a href="https://www.amnesty.org.au/activism-self-care/">Vet-kujdesi që të mund të vazhdoni të mbroni të drejtat e njeriut</a></li>
    <li><a href="https://iheartmob.org/resources/self_care">Vet-kujdesi për personat që përjetojnë ngacmim</a></li>
    <li><a href="https://www.fightcyberstalking.org/emotional-support">Mbështetja emocionale për viktimave e sulmeve/kërcënimeve në internet</a></li>
    <li><a href="https://cyber-women.com/en/self-care/">Moduli trajnimi i grave për vet-kujdes ndaj sigurisë digjitale</a></li>
    <li><a href="https://onlineharassmentfieldmanual.pen.org/self-care/">Mirëqenia dhe komuniteti</a></li>
    <li><a href="https://www.patreon.com/posts/12240673">Njëzet mënyra për të ndihmuar dikë që është duke u ngacmuar në internet</a></li>
    <li><a href="https://www.hrresilience.org/">Projekti i Qëndrueshmërisë së të Drejtave të Njeriut</a></li>
  </ul>

---

# Ndjeheni të shqetësuar?

Ngacmimet në internet, kërcënimet dhe llojet e tjera të sulmeve digjitale mund të krijojnë ndjenja të tronditjes dhe gjendje emocionale shumë delikate: mund të ndjeheni fajtorë, të turpëruar, të shqetësuar, të zemëruar, konfuz, të pafuqishëm, apo edhe mund të keni frikë për mirëqenien tuaj psikologjike ose fizike.

Nuk ka një mënyrë "të duhur" se si duhet ndier, pasi gjendja juaj e cenueshmërisë dhe çfarë do të thotë informacioni juaj personal për ju është e ndryshme nga një person te tjetri. Çdo emocion është i justifikuar, dhe nuk duhet të shqetësoheni nëse reagimi juaj është ai i duhuri apo jo.

Gjëja e parë që duhet të mbani mend është se ajo që po ndodh me ju nuk është faji juaj dhe se nuk duhet të fajësoni veten, por mundësisht të kontaktoni me dikë që i besoni që mund t'ju mbështesë në trajtimin e kësaj urgjence.

Për të zbutur një sulm në internet, do t'ju duhet të mblidhni informacione për atë që ka ndodhur, por këtë nuk keni nevojë ta bëni vetë - nëse keni një person të cilit i besoni, mund t'i kërkoni që t'ju mbështesë duke i ndjekur udhëzimet në këtë ueb-faqe, ose t'u jepni atyre qasje në pajisjet ose llogaritë tuaja për të mbledhur informacione për ju.
