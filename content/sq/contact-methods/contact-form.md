---
layout: page
title: Formulari për kontaktim
author: mfc
language: sq
summary: Metodat e kontaktimit
date: 2018-09
permalink: /sq/contact-methods/contact-form.md
parent: /sq/
published: true
---

Një formular për kontaktim ka shumë të ngjarë të mbrojë mesazhin tuaj drejtuar organizatës marrëse, në mënyrë që vetëm ju dhe organizata marrëse të mund ta lexojë atë. Megjithatë, fakti që keni kontaktuar me organizatën mund të jetë i arritshëm nga qeveritë, organet e zbatimit të ligjit ose palë të tjera me pajisjet e nevojshme teknike. Nëse dëshironi të mbroni më mirë faktin që i drejtoheni kësaj organizate, përdorni shfletuesin Tor për të hyrë në ueb-faqen me formularin për kontaktim.
