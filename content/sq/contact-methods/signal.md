---
layout: page
title: Signal
author: mfc
language: sq
summary: Metodat e kontaktimit
date: 2018-09
permalink: /sq/contact-methods/signal.md
parent: /sq/
published: true
---

Përdorimi i Signal-it do të sigurojë që përmbajtja e mesazhit tuaj të enkriptohet vetëm për organizatën marrëse, dhe vetëm ju dhe marrësi i mesazhit tuaj do të dini se komunikimi ka ndodhur. Kini parasysh se Signal e përdor numrin tuaj të telefonit si emrin tuaj të përdoruesit, kështu që do të ndani numrin tuaj të telefonit me organizatën që e kontaktoni.

Burime: [How to: Use Signal for Android](https://ssd.eff.org/en/module/how-use-signal-android), [How to: Use Signal for iOS](https://ssd.eff.org/en/module/how-use-signal-ios), [How to Use Signal Without Giving Out Your Phone Number](https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/)
