---
name: Internews
website: https://www.internews.org
logo: internews.png
languages: English, Español, Русский, العربية, Tagalog
services: in_person_training, org_security, ddos, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, forensic, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7, në nivel global
response_time: 12 orë
contact_methods: email, pgp
email: help@openinternetproject.org
pgp_key_fingerprint: 4439 FA33 F79C 2D4A 4CC8 9A4A 2FF2 08B9 BE64 58D0
initial_intake: yes
---

Duke komplementuar punën thelbësore, Internews gjithashtu punon me individë, organizata dhe komunitete në të gjithë botën për të rritur ndërgjegjësimin për sigurinë digjitale, për të mbrojtur qasjen në internet të hapur dhe të pa censuruar dhe për të përmirësuar praktikat e sigurisë digjitale. Internews ka trajnuar mijëra gazetarë dhe mbrojtës të të drejtave të njeriut në 80+ vende, dhe ka rrjete të fuqishme të trajnerëve dhe auditorëve digjitalë vendorë dhe rajonalë të njohur me kornizën e Auditimit të Sigurisë dhe Modelin e Vlerësimit për Grupet e Avokatisë (SAFETAG) (https://safetag.org), zhvillimin e të cilit e ka udhëhequr Internews. Internews ndërton partneritete të forta, të përgjegjshme me shoqërinë civile dhe firmat e inteligjencës dhe analizave në sektorin privat, dhe mund të mbështesë partnerët drejtpërdrejt në ruajtjen e një pranie të sigurt dhe të pa censuruar në internet. Internews ofron ndërhyrje teknike dhe joteknike nga vlerësimet fillestare të sigurisë duke përdorur kornizën e SAFETAG për vlerësimin e politikave organizative e deri te strategjitë e rishikuara të zbutjes së rrezikut të bazuar në hulumtimin e kërcënimeve. Ne mbështesim drejtpërdrejt analizën e “phishing” dhe “malware” për grupet e të drejtat të njeriut dhe mediave, të cilët përjetojnë sulme të shënjestruara digjitale.
