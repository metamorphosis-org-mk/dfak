---
name: Front Line Defenders
website: https://www.frontlinedefenders.org/emergency-contact
logo: FrontLineDefenders.jpg
languages: Español, English, Русский, فارسی, Français, Português, Türkçe , العربية, 中文
services: grants_funding, in_person_training, org_security, digital_support, relocation, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: hrds, hros
hours: raste urgjente 24/7, globalisht; e hënë-e premte në orët e rregullta të punës, IST (UTC + 1), stafi ndodhet në zona të ndryshme kohore në rajone të ndryshme
response_time: të njëjtën ditë ose ditën tjetër në rast emergjence
contact_methods: web_form, phone, skype, email
web_form: https://www.frontlinedefenders.org/secure/comment.php
phone: +353-1-210-0489 për raste urgjente; +353-1-212-3750 telefoni i zyrës
skype: front-line-emergency?call
email: info@frontlinedefenders.org për komente ose pyetje
initial_intake: yes
---

Front Line Defenders është një organizatë ndërkombëtare me qendër në Irlandë
që punon për mbrojtjen e integruar të mbrojtësve të të drejtave të njeriut në
rrezik. Front Line Defenders siguron mbështetje të shpejtë dhe praktike për mbrojtësit e të drejtave të njeriut në rrezik të menjëhershëm përmes granteve të sigurisë, trajnimeve të sigurisë fizike dhe digjitale, avokimit dhe fushatave.

Front Line Defenders operon një linjë telefonike mbështetëse urgjente 24/7
+ 353-121-00489 për mbrojtësit e të drejtave të njeriut në rrezik të menjëhershëm në arabisht,
anglisht, frëngjisht, rusisht ose spanjisht. Kur mbrojtësit e të drejtave të njeriut përballen me një kërcënim të menjëhershëm për jetën e tyre, Front Line Defenders mund të ndihmojë
me zhvendosje të përkohshme. Front Line Defenders ofron trajnime për sigurinë fizike dhe sigurinë digjitale. Front Line Defenders gjithashtu publikon rastet e mbrojtësve të të drejtave të njeriut në rrezik dhe zhvillon fushatë dhe avokon në nivelin ndërkombëtar duke përfshirë BE, KB, mekanizma ndër-rajonalë dhe me qeveritë direkt për mbrojtjen e tyre.
