---
name: Deflect
website: https://www.deflect.ca
logo: deflect.png
languages: English, Français, Русский, Español, Bahasa Indonesia, Filipino
services: ddos, web_hosting, web_protection
beneficiaries: hrds, cso
hours: E hënë-e premte, 24/5, UTC-4
response_time: 6 orë
contact_methods: email
email: support@equalit.ie
initial_intake: no
---

Deflect është një shërbim falas i sigurisë në internet që e mbron shoqërinë civile dhe
grupet e të drejtave të njeriut nga sulmet digjitale.
