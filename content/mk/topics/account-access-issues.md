---
layout: page
title: "Не можам да пристапам на мојот профил"
author: RaRaNet
language: mk
summary: "Дали имате проблем да пристапите до вашата е-пошта, социјалните медиуми или одреден профил на веб-страница? Дали профилот покажува активност што не ја препознавате? Постојат многу работи што можете да направите за да го решите овој проблем."
date: 2019-03
permalink: /mk/topics/account-access-issues/
parent: Home
---

# Загубив право на пристап на мојот профил

Употребата на социјалните медиуми и комуникациските медиуми е широко распространета помеѓу членовите на граѓанското општество за комуникација, споделување на знаење и застапување на нивните цели. Како последица на тоа, ваквите профили се многу често таргетирани од злонамерни чинители, кои честопати се обидуваат да ги компромитираат овие профили, предизвикувајќи штета на членовите на граѓанското општество и нивните контакти.

Ова упатство служи за да ви помогне во случај да загубите право на пристап до некој од вашите профили бидејќи тој бил компромитиран.

Во продолжение следува прашалник што ќе ви помогне да ја идентификувате природата на вашиот проблем и да пронајдете можни решенија.

## Workflow

### Password_Typo

> Понекогаш  не можеме да се логираме на нашиот профил затоа што погрешно ја внесуваме лозинката, затоа што јазикот на тастатура не е оној што вообичаено го користиме или пак затоа што имаме вклучено CapsLock.
>
> Обидете се да ги напишете вашето корисничко име и лозинка во уредувач на текст, копирајте ги оттаму и вметнете ги во полињата за најавување.

Дали овој предлог ви помогна да се најавите на вашиот профил?

- [Да](#resolved_end)
- [Не](#What_Type_of_Account_or_Service)

### What_Type_of_Account_or_Service

Кој е видот на профилот или услугата до кој/а го загубивте пристапот?

- [Facebook](#Facebook)
- [Facebook Page](#Facebook_Page)
- [Twitter](#Twitter)
- [Google/Gmail](#Google)
- [Yahoo](#Yahoo)
- [Hotmail/Outlook/Live](#Hotmail)
- [ProtonMail](#ProtonMail)
- [Instagram](#Instagram)
  <!--- - [AddOtherServiceLink](#service_Name) -->

### Facebook_Page

Дали страницата има други администратори?

- [Да](#Other_admins_exist)
- [Не](#Facebook_Page_recovery_form)

### Other_admins_exist

Дали другиот/те администратор(и) го имаат истиот проблем?

- [Да](#Facebook_Page_recovery_form)
- [Не](#Other_admin_can_help)

### Other_admin_can_help

> Побарајте другите администратори повторно да ве додадат на страницата за администратори.

Дали оваа сугестија помогна во решавањето на вашиот проблем?

- [Да](#Fb_Page_end)
- [Не](#account_end)

### Facebook_Page_recovery_form

> Најавете се на Facebook и користете го [овој формулар за враќање на страницата] (https://www.facebook.com/help/contact/164405897002583)).
>
> Ве молиме имајте на ум дека може да биде потребно подолго време за да добиете одговор на вашите барања. Зачувајте ја оваа страница во Bookmarks и вратете се на овој работен тек за неколку дена.

Дали постапката за враќање на страницата функционира?

- [Да](#resolved_end)
- [Не](#account_end)

<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

Дали имате пристап до поврзаната е-пошта/мобилен телефон за враќање на профилот?

- [Да](#I_have_access_to_recovery_email_google)
- [Не](#Recovery_Form_google)

### I_have_access_to_recovery_email_google

Проверете дали сте добиле е-пошта со предмет "Critical security alert for your linked Google Account" или СМС-порака од Google. Дали сте добиле ваква е-пошта или СМС-порака?

- [Да](#Email_received_google)
- [Не](#Recovery_Form_google)

### Email_received_google

Проверете дали во е-поштата или СМС- пораката има линк "recover your account".  

- [Има](#Recovery_Link_Found_google)
- [Нема](#Recovery_Form_google)

### Recovery_Link_Found_google

> Употребете го линкот „recover your account“ за да го вратите вашиот профил.

Дали успеавте да го вратите вашиот профил?

- [Да](#resolved_end)
- [Не](#Recovery_Form_google)

### Recovery_Form_google

> Обидете се преку [овој формулар за враќање да ја вратите вашата сметка] (https://support.google.com/accounts/answer/7682439?hl=en).
>
> Ве молиме имајте на ум дека може да биде потребно подолго време за да добиете одговор на вашите барања. Зачувајте ја оваа страница во Bookmarks и вратете се на овој работен тек за неколку дена.

Дали постапката за враќање на профилот функционира?

- [Да](#resolved_end)
- [Не](#account_end)

<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

Дали имате пристап до поврзаната е-пошта/мобилен телефон за враќање на профилот?

- [Да](#I_have_access_to_recovery_email_yahoo)
- [Не](#Recovery_Form_Yahoo)

### I_have_access_to_recovery_email_yahoo

Проверете дали сте добиле е-пошта од Yahoo со предмет „Password change for your Yahoo account“.

- [Да](#Email_received_yahoo)
- [Не](#Recovery_Form_Yahoo)

### Email_received_yahoo

Проверете дали во е-поштата го има следниот линк "Recover your account here".

- [Да](#Recovery_Link_Found_Yahoo)
- [Не](#Recovery_Form_Yahoo)

### Recovery_Link_Found_Yahoo

> Употребете го линкот "Recover your account here" за да го вратите вашиот профил.

Дали успеавте да го вратите профилот?

- [Да](#resolved_end)
- [Не](#Recovery_Form_Yahoo)

### Recovery_Form_Yahoo

> Ве молиме следете ги [овие упатства за да го вратите вашиот профил] (https://help.yahoo.com/kb/account/fix-problems-signing-yahoo-account-sln2051.html?impressions=true).
>
> Ве молиме имајте на ум дека може да биде потребно подолго време за да добиете одговор на вашите барања. Зачувајте ја оваа страница во Bookmarks и вратете се на овој работен тек за неколку дена.

Дали постапката за враќање на профилот функционира?

- [Да](#resolved_end)
- [Не](#account_end)

<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

Дали имате пристап до поврзаната е-пошта/мобилен телефон за враќање на профилот?

- [Да](#I_have_access_to_recovery_email_Twitter)
- [Не](#Recovery_Form_Twitter)

### I_have_access_to_recovery_email_Twitter

Проверете дали имате добиено е-пошта од Twitter со предмет "Your Twitter password has been changed".

- [Да](#Email_received_Twitter)
- [Не](#Recovery_Form_Twitter)

### Email_received_Twitter

Проверете дали во е-поштата има линк „recover your account“.

- [Има](#Recovery_Link_Found_Twitter)
- [Нема](#Recovery_Form_Twitter)

### Recovery_Link_Found_Twitter

> Употребете го линкот „recover your account“ за да го вратите вашиот профил.

Дали успеавте да го вратите вашиот профил?

- [Да](#resolved_end)
- [Не](#Recovery_Form_Twitter)

### Recovery_Form_Twitter

> Обидете се со [оваа форма за враќање да го вратите овој профил] (https://help.twitter.com/forms/restore).
>
> Ве молиме имајте на ум дека може да биде потребно подолго време за да добиете одговор на вашите барања. Зачувајте ја оваа страница во Bookmarks и вратете се на овој работен тек за неколку дена.

Дали постапката за враќање на профилот функционира?

- [Да](#resolved_end)
- [Не](#account_end)

<!---=========================================================
//Protonmail
//========================================================= -->

### ProtonMail

> Следете ги [овие инструкции за да го вратите вашиот профил](https://protonmail.com/support/knowledge-base/reset-password/).
>
> Ве молиме имајте на ум дека може да биде потребно подолго време за да добиете одговор на вашите барања. Зачувајте ја оваа страница во Bookmarks и вратете се на овој работен тек за неколку дена.

Дали постапката за враќање на профилот функционира?

- [Да](#resolved_end)
- [Не](#account_end)

<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

Дали имате пристап до поврзаната е-пошта/мобилен телефон за враќање на профилот?

- [Да](#I_have_access_to_recovery_email_Hotmail)
- [Не](#Recovery_Form_Hotmail)

### I_have_access_to_recovery_email_Hotmail

Проверете дали сте добиле е-пошта од Hotmail со предмет "Microsoft account password change".

- [Да](#Email_received_Hotmail)
- [Не](#Recovery_Form_Hotmail)

### Email_received_Hotmail

Проверете дали во е-поштата има линк "Reset your password".

- [Има](#Recovery_Link_Found_Hotmail)
- [Нема](#Recovery_Form_Hotmail)

### Recovery_Link_Found_Hotmail

> Употребете го линкот „Reset your password“ за да го вратите вашиот профил.

Дали успеавте да го вратите вашиот профил преку линкот "Reset your password"?

- [Да](#resolved_end)
- [Не](#Recovery_Form_Hotmail)

### Recovery_Form_Hotmail

> Обидете се со [овој формулар за враќање на вашиот профил](https://account.live.com/acsr).
>
> Ве молиме имајте на ум дека може да биде потребно подолго време за да добиете одговор на вашите барања. Зачувајте ја оваа страница во Bookmarks и вратете се на овој работен тек за неколку дена.

Дали постапката за враќање на профилот функционира?

- [Да](#resolved_end)
- [Не](#account_end)

### Facebook

Дали имате пристап до поврзаната е-пошта/мобилен телефон за враќање на профилот?

- [Да](#I_have_access_to_recovery_email_Facebook)
- [Не](#Recovery_Form_Facebook)

### I_have_access_to_recovery_email_Facebook

Проверете дали сте добиле е-пошта од Facebook со предмет "Facebook password change".

- [Да](#Email_received_Facebook)
- [Не](#Recovery_Form_Facebook)

### Email_received_Facebook

Дали во таа е-пошта има порака со линк која гласи "If you didn't do this, please secure your account"?

- [Да](#Recovery_Link_Found_Facebook)
- [Не](#Recovery_Form_Facebook)

### Recovery_Link_Found_Facebook

> Употребете го линкот „Recover your account here“ за да го вратите вашиот профил.

Дали успеавте да го вратите вашиот профил со кликнување на линкот?

- [Да](#resolved_end)
- [Не](#Recovery_Form_Facebook)

### Recovery_Form_Facebook

> Обидете се со [оваа форма за враќање на вашиот профил](https://www.facebook.com/login/identify).
>
> Ве молиме имајте на ум дека може да биде потребно подолго време за да добиете одговор на вашите барања. Зачувајте ја оваа страница во Bookmarks и вратете се на овој работен тек за неколку дена.

Дали постапката за враќање на профилот функционира?

- [Да](#resolved_end)
- [Не](#account_end)

<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== not yet tested-->

### Instagram

Дали имате пристап до поврзаната е-пошта/мобилен телефон за враќање на профилот?

- [Да](#I_have_access_to_recovery_email_Instagram)
- [Не](#Recovery_Form_Instagram)

### I_have_access_to_recovery_email_Instagram

Проверете дали имате добиено е-пошта од Instagram со предмет „Your Instagram password has been changed“.

- [Да](#Email_received_Instagram)
- [Не](#Recovery_Form_Instagram)

### Email_received_Instagram

Проверете дали во е-поштата има линк за враќање на профилот.

- [Има](#Recovery_Link_Found_Instagram)
- [Нема](#Recovery_Form_Instagram)

### Recovery_Link_Found_Instagram

> Употребете го линкот „Recover your account here“ за да го вратите вашиот профил.

Дали успеавте да го вратите вашиот профил?

- [Да](#resolved_end)
- [Не](#Recovery_Form_Instagram)

### Recovery_Form_Instagram

> Обидете се да ги [следите овие инструкции за да го вратите вашиот профил](https://help.instagram.com/149494825257596?helpref=search&sr=1&query=hacked).
>
> Ве молиме имајте на ум дека може да биде потребно подолго време за да добиете одговор на вашите барања. Зачувајте ја оваа страница во Bookmarks и вратете се на овој работен тек за неколку дена.

Дали постапката за враќање на профилот функционира?

- [Да](#resolved_end)
- [Не](#account_end)

### Fb_Page_end

Навистина сме среќни што вашиот проблем е решен. Ве молиме прочитајте ги овие препораки што ќе ви помогнат да ја минимизирате можноста во иднина да изгубите пристап до вашата страница:

- Активирајте двофакторска автентикација 2FA за сите администратори на страницата.
- Доделете улога на администратор само на луѓе на кои им верувате и кои се подготвени да реагираат.

### account_end

Ако постапките што се предложени во овој  работен тек не ви помогнале да го вратите пристапот до вашиот профил, можете да им се обратите на следниве организации за да побарате дополнителна помош:

:[](organisations?services=account)

### resolved_end

Се надеваме дека овој водич на ДПП беше корисен. Ве молиме да ни дадете повратни информации [преку е-пошта] (mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Ве молиме прочитајте ги овие препораки за да ви помогнат да ја минимизирате можноста во иднина да загубите пристап до вашите профили.

- Секогаш е добро да вклучите двофакторска автентикација (2FA) за сите ваши профили што ја поддржуваат оваа опција.
- Никогаш не ја користете истата лозинка за повеќе од еден профил. Ако во моментов го правите тоа, треба да ги промените лозинките и да користите посебна и единствена лозинка за секоја од вашите профили.
- Употребата на менаџер за лозинки ќе ви помогне да креирате и запомните уникатни, силни лозинки за сите ваши профили.
- Бидете внимателни кога користите јавни и непроверени вифи мрежи и доколку можете приклучете се преку VPN или Tor.

#### Ресурси

- [Безбедност во кутија - Креирајте и одржувајте силни лозинки](https://securityinabox.org/en/guide/passwords/)
- [Безбедносна самоодбрана - Како да се заштитите на социјалните мрежи ](https://ssd.eff.org/en/module/protecting-yourself-social-networks)

<!--- Edit the following to add another service recovery workflow:
#### service_name

Do you have access to the connected recovery email/mobile?

- [Yes](#I_have_access_to_recovery_email_google)
- [No](#Recovery_Form_google)

### I_have_access_to_recovery_email_google

Check if you received a "[Password Change Email Subject]" email from service_name. Did you receive it?

- [Yes](#Email_received_service_name)
- [No](#Recovery_Form_service_name

### Email_received_service_name

> Please check if there is a "recover your account" link. Is it there?

- [Yes](#Recovery_Link_Found_service_name)
- [No](#Recovery_Form_service_name)

### Recovery_Link_Found_service_name

> Please use the [Recovery Link Description](URL) link to recover your account.

Were you able to recover your account with "[Recovery Link Description]" link?

- [Yes](#resolved_end)
- [No](#Recovery_Form_service_name)

### Recovery_Form_service_name

> Please try this recovery form to recover this account: [Link to the standard recovery form].
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)

-->
