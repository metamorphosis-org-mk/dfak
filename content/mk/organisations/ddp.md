---
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: Понеделник-четврток 9-17 ч. средноевропско време
response_time: 4 дена
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

Целта на Digital Defenders Partnership (DDP)  е да ја заштитува и унапредува слободата на интернет и да го чува интернетот безбеден од појава на закани, особено во репресивни средини. Координираме итна поддршка за поединци и организации, како на пример бранители на човековите права, новинари, активисти од граѓанското општество и блогери. Имаме пристап насочен кон луѓето, фокусирајќи се на нашите основни вредности на транспарентност, човекови права, вклученост и разновидност, еднаквост, доверливост и слобода. DDP е формирана во 2012 година од страна на Freedom Online Coalition (FOC).

DDP има три различни видови на финансирање кои се однесуваат на итни вонредни ситуации, како и долгорочни грантови насочени кон градење на капацитетите во рамките на една организација. Исто така, ние координираме стипендија за дигитален интегритет (Digital Integrity Fellowship) каде организациите добиваат персонализирани обуки за дигитална безбедност и заштита на приватноста и програма за Мрежа за брза реакција.
