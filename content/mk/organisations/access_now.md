---
name: Access Now
website: https://www.accessnow.org/help
logo: accessnow.png
languages: English, Español, Français, Deutsch, Português, Русский, العربية, Tagalog, Italiano
services: grants_funding, in_person_training, org_security, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, harassment, forensic, advocacy
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7, глобално
response_time: 2 часа
contact_methods: email, pgp
email: help@accessnow.org
pgp_key: https://keys.accessnow.org/help.asc
pgp_key_fingerprint: 6CE6 221C 98EC F399 A04C 41B8 C46B ED33 32E8 A2BC
initial_intake: yes
---

Линијата за помош на Access Now работи со поединци и организации ширум светот со цел тие да бидат безбедни на интернет. Доколку сте на ризик, можеме да ви помогнеме да ги подобрите вашите дигитални безбедносни практики за да не претрпите штета . Ако веќе сте нападнати, ви нудиме итна помош преку брза реакција.
