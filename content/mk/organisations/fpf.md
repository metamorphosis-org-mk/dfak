---
name: Freedom of the Press Foundation
website: https://freedom.press
logo: Freedom_of_the_Press_Foundation.png
languages: English
services: in_person_training, org_security, assessment, secure_comms, device_security, browsing, account, harassment, advocacy
beneficiaries: journalists, cso
hours: Понеделник до петок, работни часови, Источна временска зона, САД
response_time: 1 ден
contact_methods: web_form, email, pgp, signal, telegram
web_form: https://freedom.press/training/request-training/
email: training@freedom.press
pgp: 0x83F347CEC0095C15EBE7A8A5DC84CA3789C17673
signal: +1 (337) 401-4082
initial_intake: yes
---

Freedom of the Press Foundation (FPF) е 501 (c)3 непрофитна организација која го штити, брани и зајакнува новинарство од јавен интерес во 21 век. FPF обучува големи медиумски организации, но и независни новинари за најразлични алатки и техники за безбедност и приватност за подобро да се заштитат себеси, своите извори и своите организации.
