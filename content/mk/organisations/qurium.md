---
name: Qurium Media Foundation
website: https://www.qurium.org
logo: qm_logo.png
languages: English, Español, Français, Русский
services: web_hosting, web_protection, assessment, vulnerabilities_malware, ddos, triage, censorship, forensic
beneficiaries: journalists, media, hrds, activists, lgbti, cso
hours: 8-18 ч. понеделник - недела CET
response_time: 4 часа
contact_methods: email, pgp, web_form
web_form: https://www.qurium.org/contact/
email: info@virtualroad.org
pgp_key: https://www.virtualroad.org/keys/info.asc
pgp_key_fingerprint: 02BF 7460 09F9 40C5 D10E B471 ED14 B4D7 CBC3 9CF3
initial_intake: yes
---

Qurium Media Foundation е провајдер на решенија за безбедност за независни медиуми, организации за човекови права, новинари-истражувачи и активисти. Qurium обезбедува низа професионални, прилагодени и безбедни решенија со лична поддршка на организации и лица изложени на ризик, вклучувајќи:

- Безбеден хостинг со DDoS намалување на веб-страници со ризик
- Поддршка преку брза реакција за организации и лица кои се под непосредна закана
- Безбедносни ревизии на веб-услуги и мобилни апликации
- Решавање на блокирани веб-страници на интернет
- Форензични истраги за дигитални напади, измамнички апликации, таргетиран злонамерен софтвер и дезинформации
