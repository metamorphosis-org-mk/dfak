---
name: Internews
website: https://www.internews.org
logo: internews.png
languages: English, Español, Русский, العربية, Tagalog
services: in_person_training, org_security, ddos, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, forensic, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7, глобално
response_time: 12 часа
contact_methods: email, pgp
email: help@openinternetproject.org
pgp_key_fingerprint: 4439 FA33 F79C 2D4A 4CC8 9A4A 2FF2 08B9 BE64 58D0
initial_intake: yes
---

Надополнувајќи ја основната функција на интернетот, Internews исто така соработува со поединци, организации и заедници низ целиот свет за да ја зголеми свеста за дигиталната безбедност, да го заштити пристапот до отворен и нецензуриран интернет и да ги подобри практиките за дигитална безбедност. Internews обучи илјадници новинари и бранители на човековите права во повеќе од 80 земји и има силни мрежи на локални и регионални обучувачи за дигитална безбедност и ревизори запознаени со рамката за ревизија на безбедноста и образецот за евалузација за застапничките групи (Security Auditing Framework and Evaluation Template for Advocacy Groups) (SAFETAG)(https://safetag.org) чие креирање го водеше Internews. Internews гради силни, одговорни партнерства со граѓанското општество и со фирмите за анализа на информации за закана и од приватниот и од јавниот сектор  и може да ги поддржува партнерите директно преку одржувањето на онлајн, безбедно и нецензурирано присуство. Internews нуди технички и нетехнички интервенции  и основни проценки за безбедност користејќи ја рамката SAFETAG,  проценки на организациската политика и ревидирани стратегии за ублажување засновани на истражување за закани. Ние директно ја поддржуваме анализата на фишинг и злонамерен софтвер како промовирање на човековите права и групи на медиуми кои истпо така се соочуваат со таргетирани дигитални напади.
