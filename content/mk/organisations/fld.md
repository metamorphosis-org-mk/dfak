---
name: Front Line Defenders
website: https://www.frontlinedefenders.org/emergency-contact
logo: FrontLineDefenders.jpg
languages: Español, English, Русский, فارسی, Français, Português, Türkçe , العربية, 中文
services: grants_funding, in_person_training, org_security, digital_support, relocation, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: hrds, hros
hours: итни случаи 24/7, глобално; работни часови од понеделик до петок, IST (UTC+1), персоналот се наоѓа во различни временски зони во различни региони
response_time: истиот или следниот ден при итен случај
contact_methods: web_form, phone, skype, email
web_form: https://www.frontlinedefenders.org/secure/comment.php
phone: +353-1-210-0489 за итни случаи; +353-1-212-3750 телефонски број во канцеларија
skype: front-line-emergency?call
email: info@frontlinedefenders.org за коментари или прашања
initial_intake: yes
---

Front Line Defenders е меѓународна организација со седиште во Ирска
што работи за интегрирана заштита на бранителите на човековите права подложни на
ризик. Front Line Defenders им обезбедува брза и практична поддршка на бранителите на човекови права подложни на ризик преку грантови за безбедност, обуки за физичка и дигитална безбедност, застапување и кампањи.

Front Line Defenders работат со телефонска линија за поддршка за итни случаи 24/7
+353-121-00489 за бранителите на човекови права кои се во непосреден ризик на арапски,
англиски, француски, руски или шпански. Кога бранителите на човековите права се соочуваат со непосредна закана за нивните животи, Front Line Defenders може да помогне со
привремена релокација. Front Line Defenders нуди обука за физичка и дигитална безбедност. Front Line Defenders исто така ги објавува случаите кога бранителите на човековите права се соочуваат со ризик и спроведува кампањи и застапништва за нивна заштита на меѓународно ниво, вклучително и ЕУ, ООН, меѓурегионалните механизми и директно со засегнатите власти .
