---
name: Greenhost
website: https://greenhost.net
logo: greenhost.jpg
languages: English, Deutsch, Nederlands
services: org_security, web_hosting, web_protection, assessment, secure_comms, vulnerabilities_malware, browsing, ddos
beneficiaries: activists, lgbti, women, youth, cso
hours: 24/7, UTC+2
response_time: 4 часа
contact_methods: web_form, email, pgp, mail, phone
web_form: https://greenhost.net/about-us/contact/
email: support@greenhost.nl
pgp_key_fingerprint: 37CD 8929 D4F8 82B0 8F66 18C3 0473 77B4 B864 2066
phone: +31 20 489 04 44
mail: Johan van Hasseltkade 202, Amsterdam Noord, 1032 LP Amsterdam, Netherlands
initial_intake: yes
---

Greenhost  нуди ИТ услуги со етички и одржлив пристап. Услугите кои ги нудиме вклучуваат веб-хостинг, решенија преку облак и моќни специјализирани понуди за информациска безбедност. Соработувајќи со културолошки организации и технички пионери, ние се стремиме на нашите корисници да им ги обезбедиме сите можности на интернет но истовремено заштитувајќи ја нивната приватност. Ние сме активно вклучени во развојот на отворен извор и учествуваме во разни проекти во областа на технологијата, новинарството, културата, образованието, одржливоста и слободата на интернет.
