---
name: Deflect
website: https://www.deflect.ca
logo: deflect.png
languages: English, Français, Русский, Español, Bahasa Indonesia, Filipino
services: ddos, web_hosting, web_protection
beneficiaries: hrds, cso
hours: Понеделник-петок, 24/5 UTC-4
response_time: 6 часа
contact_methods: email
email: support@equalit.ie
initial_intake: no
---

Deflect е бесплатна услуга за безбедност на веб-страници која го брани граѓанското општество и
групи што се залагаат за човековите права од дигитални напади.
