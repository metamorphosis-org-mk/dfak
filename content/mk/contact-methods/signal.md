---
layout: page
title: Signal
author: mfc
language: mk
summary: Начини за контакт
date: 2018-09
permalink: /mk/contact-methods/signal.md
parent: /mk/
published: true
---

Користењето на Signal ќе обезбеди содржината на вашата порака да биде енкриптирана само за организацијата примател и само вие и вашиот примател ќе знаете дека се случила комуникација. Имајте на ум дека Signal го користи вашиот телефонски број како ваше корисничко име, така што ќе го споделувате вашиот телефонски број со организацијата до која се обраќате.

Ресурси: [How to: Use Signal for Android](https://ssd.eff.org/en/module/how-use-signal-android), [How to: Use Signal for iOS](https://ssd.eff.org/en/module/how-use-signal-ios), [How to Use Signal Without Giving Out Your Phone Number](https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/)