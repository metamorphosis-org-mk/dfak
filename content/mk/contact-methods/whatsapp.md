---
layout: page
title: WhatsApp
author: mfc
language: mk
summary: Начини за контакт
date: 2018-09
permalink: /mk/contact-methods/whatsapp.md
parent: /mk/
published: true
---

Доколку користите WhatsApp, вашиот разговор со примателот е заштитен, така што само вие и примателот можат да го прочитате разговорот, но фактот дека вие сте искомуницирале со примателот, може да биде достапен од властите или правните тела.

Ресурси: [How to: Use WhatsApp on Android](https://ssd.eff.org/en/module/how-use-whatsapp-android) [How to: Use WhatsApp on iOS](https://ssd.eff.org/en/module/how-use-whatsapp-ios).
