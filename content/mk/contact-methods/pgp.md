---
layout: page
title: PGP
author: mfc
language: mk
summary: Начини за контакт
date: 2018-09
permalink: /mk/contact-methods/pgp.md
parent: /mk/
published: true
---

PGP (Pretty Good Privacy) и неговиот еквивалент со отворен извор, GPG (Gnu Privacy Guard), ви овозможуваат да ги енкриптирате содржините на е-поштата за да ја заштитите вашата порака од вашиот провајдер на е-пошта или која било друга страна која може да има пристап до е-поштата. Сепак, фактот дека сте испратиле порака до организацијата примател може да биде достапна од властите или правните органи. За да го спречите ова, можете да создадете алтернативна е-пошта што не е поврзана со вашиот идентитет.

Ресурси: [Encrypting Email with Mailvelope: A Beginner's Guide](https://freedom.press/training/encrypting-email-mailvelope-beginners-guide/)