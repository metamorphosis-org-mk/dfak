---
layout: page
title: Е-пошта
author: mfc
language: mk
summary: Начини за контакт
date: 2018-09
permalink: /mk/contact-methods/email.md
parent: /mk/
published: true
---

Содржината на вашата порака како и фактот дека сте ја контактирале организацијата може да бидат достапни од владите или правните органи.