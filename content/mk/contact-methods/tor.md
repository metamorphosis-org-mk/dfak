---
layout: page
title: Tor
author: mfc
language: mk
summary: Начини за контакт
date: 2018-09
permalink: /mk/contact-methods/tor.md
parent: /mk/
published: true
---

Пребарувачот Tor  е веб-прелистувач фокусиран на приватност кој ви овозможува да комуницирате со веб-страниците анонимно со тоа што не ја споделуваат вашата локација (преку вашата IP адреса) кога ќе пристапите на веб-страницата.

Ресурси: [Overview of Tor](https://www.torproject.org/about/overview.html.en).
