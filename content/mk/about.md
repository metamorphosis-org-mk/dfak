---
layout: page.pug
title: "За"
language: mk
summary: "За Дигитална прва помош."
date: 2019-03-13
permalink: /mk/about/
parent: Home
---

Дигитална прва помош е продукт на соработката помеѓу [RaReNet](Мрежа за брза реакција)(https://www.rarenet.org/) и [CiviCERT](https://www.civicert.org/). Македонската и албанската верзија на овој продукт се направени со поддршка на Information Safety & Capacity [(ISC)](https://www.iscproject.org/) Project а реализирани од Фондација Метаморфозис.

Мрежата за брза реакција (Rapid Response Network) е меѓународна мрежа на брза реакција и шампион за дигитална безбедност, која ги вклучува  EFF, Global Voices, Hivos & the Digital Defenders Partnership, Front Line Defenders, Internews, Freedom House, Access Now, Virtual Road, CIRCL, Open Technology Fund, Greenhost како и индивидуални експерти за безбедност кои работат во областа на дигиталната безбедност и брза реакција.

Некои од овие организации и поединци се дел од CiviCERT, меѓународна мрежа за помош за дигитална безбедност и провајдери на инфраструктура, со главен фокус кон поддршка на групи и организации кои се стремат кон социјална правда и одбрана на човековите и дигиталните права. CiviCERT претставува професионално обликување на дистрибуираните напори на заедницата за брзо реагирање CERT (Computer Emergency Response Team). CiviCERT е акредитирана од Trusted Introducer, европската мрежа на доверливи тимови за брза реакција.

Дигитална прва помош е исто така [open-source проект и прифаќа надворешни придонеси.](https://gitlab.com/rarenet/dfak)

Доколку сакате да ја користите оваа страница во контексти каде што интернет конекцијата е ограничена или на места каде нема интернет, можете да преземете офлајн верзија [тука](https://digitalfirstaid.org/dfak-offline.zip).

Вашите коментари, сугестии или прашања за Дигитална прва помош можете да ги испратите на: dfak @ digitaldefenders . org

GPG - Fingerprint: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
