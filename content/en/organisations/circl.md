---
name: Computer Incident Response Center Luxembourg
website: https://circl.lu
logo: circl.png
languages: English, Deutsch, French, Luxembourgish
services: org_security, vulnerabilities_malware, forensic
beneficiaries: activists, lgbti, women, youth, cso
hours: office hours, UTC+2
response_time: 4 hours
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.circl.lu/contact/
email: info@circl.lu
pgp_key_fingerprint: CA57 2205 C002 4E06 BA70 BE89 EAAD CFFC 22BD 4CD5
phone: +352 247 88444
mail: 16, bd d'Avranches, L-1160 Luxembourg, Grand-Duchy of Luxembourg
initial_intake: yes
---

CIRCL is the CERT for the private sector, communes and non-governmental entities in Luxembourg.

CIRCL provides a reliable and trusted point of contact for any users, companies and organizations based in Luxembourg, for the handling of attacks and incidents. Its team of experts acts like a fire brigade, with the ability to react promptly and efficiently whenever threats are suspected, detected or incidents occur.

CIRCL’s aim is to gather, review, report and respond to cyber threats in a systematic and prompt manner.
